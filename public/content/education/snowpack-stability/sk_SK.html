<div>
  <h1>Stabilita snehovej pokrývky</h1>

  <section class="section-centered">
    <div class="panel">
      <p>
        Stabilita snehovej pokrývky je spolu s
        <a href="/education/frequency">množstvom nebezpečných svahov</a> a
        <a href="/education/avalanche-sizes">veľkosťou lavín</a> jedným z
        kľúčových údajov pri hodnotení úrovne
        <a href="/education/danger-scale">lavínového nebezpečenstva</a> pomocou
        <a href="/education/matrix">matice EAWS</a>. Na valnom zhromaždení EAWS
        v Davose v júni 2022 boli všetky tri premenné zadefinované a podložené
        príkladmi používanými v praxi. Definície, poznámky, príklady a
        podrobnejšie informácie o premenlivej stabilite snehovej pokrývky
        nájdete v nasledujúcich častiach alebo na
        <a href="www.avalananches.org/standards/">avalanches.org</a>
      </p>

      <h6>Definícia</h6>

      <p>
        Stabilita snehovej pokrývky je lokálna vlastnosť snehovej pokrývky,
        ktorá popisuje náchylnosť zasneženého svahu k uvoľneniu lavíny
        <a
          href="https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2018GL078069"
          >(Reuter a Schweizer, 2018)</a
        >. Stabilita snehovej pokrývky je opísaná pomocou štyroch tried:
        <i>veľmi slabá</i>, <i>slabá</i>, <i>stredná</i> a <i>dobrá</i>.
      </p>

      <h6>Doplnok</h6>

      <p>Podľa typu lavíny môže byť stabilita snehovej pokrývky popísaná:</p>

      <ul>
        <li>
          Iniciácia zlomu, šírenie trhliny a podpora ťahom (dosková lavína)
          <a
            href="https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2018GL078069"
            >(Reuter a Schweizer, 2018)</a
          >
        </li>

        <li>
          Strata pevnosti/spojenia (lavína z voľného snehu) (napr. McClung a
          Schaerer, 2006)
        </li>

        <li>
          Strata bazálneho trenia a podpora ťahom alebo tlakom (kĺzavá
          /základová lavína)
          <a
            href="https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2012GL052479"
            >(napr. Bartelt et al., 2012)</a
          >
        </li>
      </ul>
      <p>
        Stabilita snehovej pokrývky je nepriamo úmerná pravdepodobnosti
        uvoľneniu lavíny. Stabilita snehovej pokrývky vyjadruje, ako ľahko je
        možné snehovú pokrývku uvoľniť v dôsledku špecifického spúšťača
        <a href="https://link.springer.com/article/10.1007/s11069-017-3070-5"
          >(Statham et al., 2018)</a
        >, napríklad osoby, ktorá lyžuje svah. Tabuľka 1 ukazuje vzťah medzi
        stabilitou snehovej pokrývky a tým, aké ľahké je uvoľniť lavínu.
      </p>

      <p class="small" style="text-align: center">
        Tabuľka 1 : Triedy stability a ich typické uvoľnenia lavíny.
      </p>

      <div class="table-container">
        <table class="pure-table pure-table-striped full-width">
          <thead>
            <tr>
              <th>Trieda stability</th>

              <th>Aké ľahké je uvoľniť lavínu?</th>
              stabilita sneho
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Veľmi slabá</td>

              <td>Samovoľné lavíny / veľmi ľahké uvoľniť lavínu</td>
            </tr>
            <tr>
              <td>Slabá</td>

              <td>
                Ľahké uvoľniť lavínu (malým dodatočným zaťažením, napríklad
                jeden lyžiar)
              </td>
            </tr>
            <tr>
              <td>Stredná</td>

              <td>
                Obtiažne uvoľnenie lavíny (veľkým dodatočným zaťažením,
                napríklad výbušninami)
              </td>
            </tr>
            <tr>
              <td>Dobrá</td>

              <td>Stabilné podmienky</td>
            </tr>
          </tbody>
        </table>
      </div>

      <p>
        Termín lokálny sa vzťahuje na bod, ktorého veľkosť sa pohybuje od
        potenciálneho miesta uvoľnenia alebo testu stability po štartovaciu zónu
        lavíny.
      </p>

      <p>
        Všetky hodnotenia stability snehovej pokrývky sa môžu vzťahovať na
        budúcnosť (predpoveď) alebo súčasnosť (teraz) na základe pozorovaní
        alebo modelov. Napríklad, ak sa snehová pokrývka v oblasti uvoľnenia
        považuje dnes za <i>strednú</i>, a zajtra sa očakáva vrstva nového
        snehu, je potrebné posúdiť stabilitu snehovej pokrývky vrátane novej
        snehovej vrstvy – teda posúdiť stabilitu zajtra. Do tej doby sa
        pravdepodobne stabilita zmení na <i>slabú</i> alebo dokonca
        <i>veľmi slabú</i>.
      </p>

      <h6>
        Vzťah medzi triedami stability snehovej pokrývky a typickými javmi alebo
        pozorovaniami
      </h6>

      <p>
        Obrázky 1 až 3 poskytujú prehľad vzťahu medzi triedami stability
        snehovej pokrývky a typickými javmi alebo pozorovaniami závislými od
        troch hlavných typov uvoľňovania lavín (lavíny zo suchého, mokrého
        snehu).
      </p>

      <p>
        Na obrázku 1 sú uvedené príklady bežných typických znakov alebo indícií
        pre rôzne triedy stability snehovej pokrývky so zameraním na uvoľňovanie
        lavín zo suchého snehu. Najznámejšími príkladmi sú spontánne lavíny,
        prípadne znaky nestability (whump) alebo klasické testy stability
        snehovej pokrývky, ako napríklad rozšírený kompresný test (ECT) alebo
        test zosuvného bloku (RB). Šípky naznačujú, že existencia smerom k
        nižším triedam stability je nevyhnutná. Samovoľné lavíny sú jasnou
        indikáciou pre <i>VEĽMI SLABÚ</i> triedu stability, zatiaľ čo malé a
        veľké dodatočné zaťaženie sa považujú za ekvivalent <i>SLABEJ</i> a
        <i>STREDNEJ</i> triedy stability snehovej pokrývky. Pozorovania a
        výsledky testov stability by sa mali považovať len za orientačné.
      </p>

      <p>
        Skratky na obrázku: Skratky: Rozšírený kompresný test (ECT), Zostuvný
        blok(RB), celý blok (wB), čiastočné uvoľnenie (pR).
        <a href="https://tc.copernicus.org/articles/15/3293/2021/"
          >*Schweizer et al. (2021)</a
        >,
        <a href="https://nhess.copernicus.org/articles/20/1941/2020/"
          >**Techel et al. (2020)</a
        >, +1 lyžiar bez pádov,uvoľnené lyžovaním, ++pád 1 lyžiara, skupina
        lyžiarov bez rozostupov, peší výstup.
      </p>

      <table style="width: 100%; border: 0px solid #000; text-align: center">
        <tbody>
          <tr>
            <td>
              <img
                style="width: 100%"
                title="Príklady bežných znakov alebo indikácií stability pri suchej snehovej pokrývke."
                alt="Príklady bežných znakov alebo indikácií stability pri suchej snehovej pokrývke."
                data-entity-type="file"
                data-entity-uuid="d397a528-6f80-4ea3-afdf-255ab9ae681d"
                src="/content_files/ExamplesStabilityClassesDrySnowSlabAvalanches_SK.png"
              />
            </td>
          </tr>
        </tbody>
      </table>
      <p class="small" style="text-align: center">
        Obrázok 1: Príklady bežných znakov alebo indikácií stability pri suchej
        snehovej pokrývke.
      </p>

      <p>
        Pri stabilite na mokrom snehu a stabilite na kĺzavom snehu je často
        ťažké rozlíšiť triedy <i>"STREDNÁ"</i>a <i>"SLABÁ"</i>. Lavíny z mokrého
        snehu sa najčastejšie uvoľňujú prirodzene, a preto sa spájajú s triedou
        <i>„VEĽMI SLABÁ“.</i> Pozorovania často poskytnú len tendenciu k
        <i>DOBREJ</i> alebo <i>VEĽMI SLABEJ</i> stabilite (obrázky 2 a 3). Testy
        stability vo všeobecnosti fungujú v mokrom snehu zle. Testy stability by
        mali byť zohľadnené len v prípade, že poukazujú na
        <i>VEĽMI SLABÚ</i> stabilitu.
      </p>

      <table style="width: 100%; border: 0px solid #000; text-align: center">
        <tbody>
          <tr>
            <td>
              <img
                style="width: 100%"
                title="Obrázok 2: Príklady bežných znakov alebo indikácií stability na mokrom snehu."
                alt="Obrázok 2: Príklady bežných znakov alebo indikácií stability na mokrom snehu."
                data-entity-type="file"
                data-entity-uuid="d397a528-6f80-4ea3-afdf-255ab9ae681d"
                src="/content_files/ExamplesStabilityClassesWetSnowAvalanches_SK.png"
              />
            </td>
          </tr>
        </tbody>
      </table>
      <p class="small" style="text-align: center">
        Obrázok 2: Príklady bežných znakov alebo indikácií stability na mokrom
        snehu.
      </p>

      <p>
        Na obrázku 2 sú uvedené bežné znaky alebo indikácie typické pre rôzne
        triedy stability na mokrom snehu. Ak v snehovej pokrývke nie je prítomná
        tekutá voda, lavíny z mokrého snehu nie sú možné.
      </p>

      <table style="width: 100%; border: 0px solid #000; text-align: center">
        <tbody>
          <tr>
            <td>
              <img
                style="width: 100%"
                title="Obrázok 3: Príklady bežných znakov alebo indikácií pre stabilitu na kĺzavom snehu."
                alt="Obrázok 3: Príklady bežných znakov alebo indikácií pre stabilitu na kĺzavom snehu."
                data-entity-type="file"
                data-entity-uuid="d397a528-6f80-4ea3-afdf-255ab9ae681d"
                src="/content_files/ExamplesStabilityClassesGlideSnowAvalanches_SK.png"
              />
            </td>
          </tr>
        </tbody>
      </table>
      <p class="small" style="text-align: center">
        Obrázok 3: Príklady bežných znakov alebo indikácií pre stabilitu na
        kĺzavom snehu.
      </p>

      <p>
        Na obrázku 3 sú uvedené bežné znaky alebo indikácie typické pre
        stability pri kĺzaní na snehu. Kĺzavé snehové lavíny nie sú možné, ak na
        rozhraní snehu a pôdy nie je prítomná tekutá voda.
      </p>
    </div>
  </section>
  <section classname="section-centered section-context">
    <div classname="panel">
      <h2 classname="subheader">Vzdelávanie &amp; Prevencia</h2>

      <ul classname="list-inline list-buttongroup-dense">
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/danger-scale"
            title="Stupnica lavínového nebezpečenstva"
            >Stupnica lavínového nebezpečenstva</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/avalanche-problems"
            title="Typické lavínové problémy"
            >Typické lavínové problémy</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/workflow"
            title="Pracovný postup"
            >Pracovný postup</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/matrix"
            title="EAWS Matica"
            >EAWS Matica</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/frequency"
            title="Množstvo nebezpečných miest"
            >Množstvo nebezpečných miest</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/avalanche-sizes"
            title="Veľkost lavín"
            >Veľkost lavín</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/spatio-temporal-scale"
            title="Stanovenie priestorovej a časovej mierky"
            >Stanovenie priestorovej a časovej mierky</a
          >
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/danger-patterns"
            title="Modely lavínového nebezpečenstva "
            >Modely lavínového nebezpečenstva
          </a>
        </li>
        <li>
          <a
            classname="secondary pure-button pure-buton-wrap"
            href="/education/handbook"
            title="Príručka"
            >Príručka</a
          >
        </li>
      </ul>
    </div>
  </section>
</div>
