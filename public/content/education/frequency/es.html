<div>
<h1>Distribución de frecuencias de la estabilidad del manto nivoso</h1>&#13;
&#13;
<section class="section-centered"><div class="panel">&#13;
    <p><a href="/education/snowpack-stability">Snowpack stability</a>, along with&#13;
      the frequency distribution of snowpack stability and&#13;
      <a href="/education/avalanche-sizes">avalanche size</a>, is one of the key&#13;
      contributors when assessing the&#13;
      <a href="/education/danger-scale">avalanche danger level</a> using the&#13;
      <a href="/education/matrix">EAWS Matrix</a>. Since the EAWS General&#13;
      Assembly in Davos in June 2022, all three variables were defined and&#13;
      underpinned with examples used in practice. Definitions, remarks, examples&#13;
      and more in-depth information for the variable frequency distribution of&#13;
      snowpack stability can be found in the following sections or on&#13;
      <a href="www.avalananches.org/standards/">avalanches.org</a></p>&#13;
&#13;
    <h6>Definición</h6>&#13;
    <p>The frequency distribution of snowpack stability describes the percentages&#13;
      of points for each stability class relative to all points in avalanche&#13;
      terrain. Thus, the frequency <i>f</i> for all points with stability class&#13;
      <i>i</i> (n<sub><i>i</i></sub>) compared to all points (<i>n</i>) is <i>f(i)</i> = <i>n</i><sub><i>i</i></sub>/<i>n</i>. The frequency distribution of snowpack stability is described&#13;
      using four classes: <i>Many</i>, <i>Some</i>, <i>A Few</i>, and&#13;
      <i>None or Nearly None</i> (Table 1).</p>&#13;
&#13;
    <h6>Observaciones</h6>&#13;
&#13;
    <p>The frequency distribution of snowpack stability refers to (many) points&#13;
      (i.e., stability tests, snowpack models or potential triggering locations)&#13;
      or avalanche starting zones.</p>&#13;
&#13;
    <p>The frequency must be assessed for a warning region which must be equal to&#13;
      or larger than the&#13;
      <a href="/education/spatio-temporal-scale">reference unit</a>.</p>&#13;
&#13;
    <p>The definition asks, in theory, for a percentage. However, this is often&#13;
      impossible to assess since the frequency distribution must often be&#13;
      inferred from sparse data in a real situation. Percentages or thresholds&#13;
      for <i>Many</i>, <i>Some</i>, <i>A Few</i>, and&#13;
      <i>None or Nearly None</i> differ depending on the measurement/evidence&#13;
      used or available (see below for more details). For instance, the&#13;
      percentages for slopes that produce spontaneous avalanches might be lower&#13;
      than the percentage of points with stability tests that indicate&#13;
      <i>Very Poor</i> stability.</p>&#13;
    <p>See&#13;
      <a href="https://www.avalanches.org/wp-content/uploads/2022/12/EAWS_matrix_definitions_EN.pdf">&#13;
        Appendix B</a>&#13;
      within the public document on EAWS for a brief overview of research&#13;
      related to frequency distributions of snowpack stability.</p>&#13;
&#13;
    <p class="small" style="text-align: center">Tabla 1: Clases de distribución de frecuencias de la estabilidad del manto nivoso.</p>&#13;
&#13;
    <div class="table-container">&#13;
      <table class="pure-table pure-table-striped full-width">
<thead><tr>
<th>Clase de frecuencia</th>&#13;
            <th>Descripción</th>&#13;
            <th>Evidencias (p.e. observaciones)</th>&#13;
          </tr></thead>
<tbody>
<tr>
<td>Muchas</td>&#13;
            <td>Los puntos con esta clase de estabilidad son abundantes.</td>&#13;
            <td>Las evidencias de la inestabilidad suelen ser fáciles de encontrar.</td>&#13;
          </tr>
<tr>
<td>Algunas</td>&#13;
            <td>Points with this stability class are neither many nor a few, but&#13;
              these points typically exist in terrain features with common&#13;
              characteristics (i.e., close to ridgelines, in gullies).</td>&#13;
            <td></td>&#13;
          </tr>
<tr>
<td>Pocos</td>&#13;
            <td>Points with this stability class are rare. While rare, their&#13;
              number is considered relevant for stability assessment.</td>&#13;
            <td>Es difícil encontrar evidencias de la inestabilidad.</td>&#13;
          </tr>
<tr>
<td>Ninguna o casi ninguna</td>&#13;
            <td>Points with this stability class do not exist, or they are so rare&#13;
              that they are not considered relevant for stability assessment.</td>&#13;
            <td></td>&#13;
          </tr>
</tbody>
</table>
</div>&#13;
&#13;
    <h2>Considerations when assigning classes of frequency distributions of&#13;
      snowpack stability</h2>&#13;
&#13;
    <p>Figure 1 illustrates the concept of frequency distribution. It shows an&#13;
      idealized case. We consider a hypothetical and idealized scenario where a&#13;
      stability assessment for points within starting zones is available. Here,&#13;
      our micro-region is defined by the contour map in panel a). For the sake&#13;
      of simplicity, we choose the reference unit to be equal to the&#13;
      micro-region. Thus, considering all points in the starting zones within&#13;
      the region independent of elevation, aspect or other possible subdivisions&#13;
      (Panel b). Panel c) shows the stability class assessed in each point.&#13;
      Panel d) shows the resulting frequency distribution of these stability&#13;
      classes. Generally, the frequency of the weakest stability class, in this&#13;
      case <i>Very Poos</i>, needs to be considered to determine the danger&#13;
      level.</p>&#13;
&#13;
    <table style="width: 100%; border: 0px solid #000; text-align: center"><tbody><tr>
<td><img style="width: 100%" title="Figure 1: Concept of frequency distribution for an idealized case." alt="Figure 1: Concept of frequency distribution for an idealized case." data-entity-type="file" data-entity-uuid="d397a528-6f80-4ea3-afdf-255ab9ae681d" src="/content_files/FrequencyDistributionExample_EN.png"></td>&#13;
        </tr></tbody></table>
<p class="small" style="text-align: center">Figura 1: Concepto de distribución de frecuencias para un caso idealizado.</p>&#13;
&#13;
    <p>The challenge of assigning percentages, or deciding on thresholds between&#13;
      classes, has been debated a lot and researchers and practitioners have no&#13;
      clear answer, yet. Few forecasters or practitioners have enough data&#13;
      available that is conclusive and evenly distributed over relevant release&#13;
      areas in the region they are assessing. Therefore, assigning a frequency&#13;
      distribution class remains an expert opinion and experience for the time&#13;
      to come. The avalanche forecasting community believes that much more data&#13;
      in combination with verification campaigns needs to be assessed before we&#13;
      can provide good answers. In the following paragraphs, we list some&#13;
      studies that tried to quantify the frequency distribution of snowpack&#13;
      stability in one way or another. The presented numbers are currently the&#13;
      best estimates we have describing the frequency distribution of snowpack&#13;
      stability. They have different bases and cannot be compared directly or&#13;
      combined to describe classes uniformly. More studies of this type need to&#13;
      be conducted in the future to provide reliable percentages for the&#13;
      frequency of snowpack stability.</p>&#13;
&#13;
    <h6>Tests de estabilidad (Rutschblock)</h6>&#13;
&#13;
    <p>Frequency classes derived by&#13;
      <a href="https://tc.copernicus.org/articles/14/3503/2020/tc-14-3503-2020.html">&#13;
        Techel et al. (2020)</a>&#13;
      for the frequency of the Rutschblock stability class very poor:</p>&#13;
&#13;
    <ul>
<li>
<i>Muchos</i>: &gt;20%</li>&#13;
      <li>
<i>Algunos</i>: &gt;4% - 20%</li>&#13;
      <li>
<i>Pocos</i>: &gt;0% - &lt;4%</li>&#13;
      <li>
<i>Ninguno o casi ninguno</i>: 0%</li>&#13;
    </ul>
<h6>Actividad de avalanchas (Ejemplo 1)</h6>&#13;
    <p>Avalanche activity, as observed from satellite images over Switzerland in&#13;
      two extreme avalanche situations in January 2018 and January 2019 (Hafner,&#13;
      2019): The proportion of potential release areas which was active (which&#13;
      avalanched) varied for a subset of 13 micro-regions between 4% and 23%&#13;
      with a mean of 13%. These micro-regions cover a surface area between 56&#13;
      and 506 km<sup>2</sup>, while the potential release area within these&#13;
      micro-regions covers between 21 and 159 km<sup>2</sup> of the surface area&#13;
      of these regions. When only considering the two most active neighboring&#13;
      aspects within these regions (e.g. from NW-N-NE), the observed maximum was&#13;
      41% of the total release area being active. These values can be considered&#13;
      representing high values for the term <i>Many</i>.</p>&#13;
&#13;
    <h6>Actividad de avalanchas (Ejemplo 2)</h6>&#13;
    <p>Based on a 15-year data set of manually mapped natural avalanches in the&#13;
      region of Davos, Switzerland (Völk, 2020), the following frequency classes&#13;
      were obtained using the approach described by&#13;
      <a href="https://tc.copernicus.org/articles/14/3503/2020/tc-14-3503-2020.html">&#13;
        Techel et al. (2020)</a>&#13;
      for the proportion of potential release areas which were active (which&#13;
      avalanched):</p>&#13;
&#13;
    <ul>
<li>
<i>Muchos</i>: &gt;2.2%</li>&#13;
      <li>
<i>Algunos</i>: &gt;0.02% - 2.2%</li>&#13;
      <li>
<i>Pocos</i>: &lt;0.02%</li>&#13;
    </ul>
<p>Note, the latter mapping approach has a comparably low detection rate&#13;
      <a href="https://tc.copernicus.org/articles/15/983/2021/">(Hafner et al. 2021)</a>. As an estimate, these class thresholds may be too low by a factor of&#13;
      around 2.</p>&#13;
  </div>&#13;
</section><section classname="section-centered section-context"><div classname="panel">&#13;
    <h2 classname="subheader">Educación &amp; Prevención</h2>&#13;
&#13;
    <ul classname="list-inline list-buttongroup-dense">
<li><a classname="secondary pure-button" href="/education/danger-scale" title="Danger Scale">Escala de Peligro</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/avalanche-problems" title="Avalanche Problems">Tamaño de aludes</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/avalanche-sizes" title="Avalanche Sizes">Tamaño de aludes</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/danger-patterns" title="Danger Patterns">Patrones de Peligro</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/spatio-temporal-scale" title="Spatio-temporal Scale">Escala espacio-temporal</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/workflow" title="Workflow">Workflow</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/snowpack-stability" title="Snowpack stability">Estabilidad del manto nivoso</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/frequency" title="Frequency">Frequency</a></li>&#13;
      <li><a classname="secondary pure-button" href="/education/handbook" title="Handbook">Manual</a></li>&#13;
    </ul>
</div>&#13;
</section>
</div>
