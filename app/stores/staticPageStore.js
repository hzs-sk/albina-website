import { APP_STORE } from "../appStore";
import { fetchText } from "../util/fetch";

export default class StaticPageStore {
  static async loadPage(url) {
    const lang = APP_STORE.language;
    const chapter = url.split("/")[0] || "";
    let urlLang = lang;
    if (lang == "sk") urlLang = "sk_SK";
    if (lang == "pl") urlLang = "en"; //we do not have pl translations for static pages like education
    url = `${import.meta.env.BASE_URL}content/${url}/${urlLang}.html`;

    const text = await fetchText(url);
    const sharable = true;
    // extract title from first <h1>...</h1>
    const titlePattern = /<h1>\s*(.*?)\s*<\/h1>/;
    const title = text.match(titlePattern)?.[1];
    const body = text.replace(titlePattern, "");
    return {
      data: {
        attributes: { title, chapter, body, sharable }
      }
    };
  }
}
